
using BlackPearl.Tests.IntegrationTests;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Net.Http;

namespace BlackPearl.Tests.Fixtures
{
    public class TestServerFixture : IDisposable
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;


        public TestServerFixture()
        {
            _server = new TestServer(new WebHostBuilder().UseEnvironment("Test").UseStartup<TestStartup>());
            _client = _server.CreateClient();
        }

        public TestServer Server => _server;
        public void Dispose()
        {
            _client.Dispose();
            _server.Dispose();
        }

    }

}