using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BlackPearl.Enums;
using BlackPearl.Model;
using BlackPearl.Services;
using BlackPearl.Services.Implementations;
using BlackPearl.Tests.Helpers;
using BlackPearl.Utils;
using FluentAssertions;
using Xunit;

namespace BlackPearl.Tests
{
    public class ExtractFilesTest
    {
        [Fact]
        public void ExtractFileXlsx()
        {
            var _filePath = ".xslx";
            var _batch = MockMessagesBatch.MsgCardIdentification();
            Dictionary<string, string> _itemsMockIdentification = MockItemsBatch.CardIdentification();
            List<Dictionary<string, string>> _itemsProcessIdentification = HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            var compareIdentification = _itemsProcessIdentification.Where(entry => _itemsMockIdentification[entry.First().Key] != entry.First().Value)
                .ToDictionary(entry => entry.First().Key, entry => entry.First().Value);
            bool result = false;
            if (compareIdentification.Count() == 0)
                result = true;

            result.Should().BeTrue();
        }

        [Fact]
        public void ExtractFileXls()
        {
            var _filePath = ".xsl";
            var _batch = MockMessagesBatch.MsgCardRecharge();
            Dictionary<string, string> _itemsMockRecharge = MockItemsBatch.CardRecharge();
            List<Dictionary<string, string>> _itemsProcessRecharge = HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            var compareIdentification = _itemsProcessRecharge.Where(entry => _itemsMockRecharge[entry.First().Key] != entry.First().Value)
                .ToDictionary(entry => entry.First().Key, entry => entry.First().Value);
            bool result = false;
            if (compareIdentification.Count() == 0)
                result = true;

            result.Should().BeTrue();
        }

        [Fact]
        public void ExtractFileExtensionInValid()
        {
            var _batch = MockMessagesBatch.MsgExtensionInvalid();
            Action act = () => BatchService.FileType(_batch.FilePath);
            act.Should().Throw<ArgumentException>().WithMessage("Extensão utilizada não suportada");
            //enviar arquivo com extensão diferente de .csv, .xls e .xlsx
            //validação: Mensagem => Extensão utilizada não suportada
        }

        [Fact]
        public void ExtractFileNonExistent()
        {
            var _filePath = ".xsl";
            var _batch = MockMessagesBatch.MsgFileNotExist();
            Action act = () =>
            {
                List<Dictionary<string, string>> _itemsProcessRecharge =
                    HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            };
            act.Should().Throw<FileNotFoundException>().WithMessage("Não foi possivel realizar o download");
            //enviar arquivo que não existe
            //validação: Mensagem => Não foi possivel realizar o download
        }

        [Fact]
        public void ExtractFileMessy()
        {
            var _filePath = ".xsl";
            var _batch = MockMessagesBatch.MsgFileMessy();
            Dictionary<string, string> _itemsMockDischarge = MockItemsBatch.CardDischarge();
            List<Dictionary<string, string>> _itemsProcessDischarge = HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            bool result = _itemsProcessDischarge[2].ContainsValue(_itemsMockDischarge.First().Value);
            result.Should().BeTrue();
            //enviar arquivo com colunas bagunçadas
            //validação: Arquivo válido até a coluna correta
        }

        [Fact]
        public void ExtractFileEmpty()
        {
            var _filePath = ".xslx";
            var _batch = MockMessagesBatch.MsgFileEmpty();
            Action act = () =>
            {
                List<Dictionary<string, string>> _itemsProcessRecharge =
                    HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            };
            act.Should().Throw<ArgumentException>().WithMessage("Arquivo não possui registros");
            //enviar arquivo vazio somento com cabecario
            //validação: Mensagem de arquivo não possui registros
        }

        [Fact]
        public void ExtractFileWithMissingData()
        {
            var _filePath = ".xsl";
            var _batch = MockMessagesBatch.MsgFileWithMissingData();
            List<Dictionary<string, string>> _itemsProcessIdentification = HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            _itemsProcessIdentification.First().Count().Should().Be(4);
            _itemsProcessIdentification.Should().HaveCount(8);
            //enviar arquivo com uma coluna preenchida e outra não 
            //e com uma linha preenchida e outra não
            //validação: Extrair todas as colunas e linhas;

        }

        [Fact]
        public void ExtractBrokedFile()
        {
            var _filePath = ".xslx";
            var _batch = MockMessagesBatch.MsgFileBrokedFile();
            Action act = () =>
            {
                List<Dictionary<string, string>> _itemsProcessRecharge =
                    HandlerFileExcel.ExtractItemsFileExcel(_batch.FilePath, _batch.BatchType, _filePath);
            };
            act.Should().Throw<DllNotFoundException>().WithMessage("Arquivo Corrompido");
            // extrair arquivos corrompidos
            //validação: Mensagem => Arquivo Corrompido
        }

        //TODO => enviar arquivo para file
        [Fact]
        public void ExtractLargeFiles()
        {
            var _filePath = ".xslx";
            var _batch = MockMessagesBatch.MsgCardIdentification();
            Dictionary<string, string> _itemsMockIdentification = MockItemsBatch.CardIdentification();
            List<Dictionary<string, string>> _itemsProcessIdentification = HandlerFileExcel.ExtractItemsFileExcel("/home/jairodrigues/Downloads/jairo2.xlsx", _batch.BatchType, _filePath);
            _itemsProcessIdentification.Should().HaveCount(10000);
            // extrair arquivos com 10.000 linhas de identificação
            //validação: lista de retorno com 10.000
        }

    }
}