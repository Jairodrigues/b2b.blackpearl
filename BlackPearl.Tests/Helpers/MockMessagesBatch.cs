using System;
using System.Collections.Generic;
using BlackPearl.Enums;
using BlackPearl.Model;

namespace BlackPearl.Tests.Helpers
{
    public static class MockMessagesBatch
    {
        public static Batch MsgCardIdentification()
        {
            return new Batch()
            {
                BatchType = BatchType.CardIdentification,
                CompanyId = 123,
                Requester = "Jairo",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_11_30_16_27_43_147_IDENTIFICACAO (1) (2).xlsx",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgCardRecharge()
        {
            return new Batch()
            {
                BatchType = BatchType.CardRecharge,
                CompanyId = 456,
                Requester = "Tiago",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_04_14_53_56_621_CargaLote.xlsx",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgExtensionInvalid()
        {
            return new Batch()
            {
                BatchType = BatchType.CardRecharge,
                CompanyId = 456,
                Requester = "Tiago",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_04_14_53_56_621_CargaLote.jpeg",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgFileNotExist()
        {
            return new Batch()
            {
                BatchType = BatchType.CardRecharge,
                CompanyId = 456,
                Requester = "Tiago",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/AchiveNotExist.xls",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgFileMessy()
        {
            return new Batch()
            {
                BatchType = BatchType.CardDischarge,
                CompanyId = 789,
                Requester = "Vanessa",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_12_13_11_45_554_DESCARGA.xlsx",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgFileEmpty()
        {
            return new Batch()
            {
                BatchType = BatchType.CardRecharge,
                CompanyId = 101,
                Requester = "Vanessa",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_12_20_59_21_165_CARGA_EMPTY.xlsx",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgFileWithMissingData()
        {
            return new Batch()
            {
                BatchType = BatchType.CardIdentification,
                CompanyId = 111,
                Requester = "Vanessa",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_12_20_59_38_427_IDENTIFICACAO_MissingData.xlsx",
                BatchStatus = BatchStatus.Created
            };
        }

        public static Batch MsgFileBrokedFile()
        {
            return new Batch()
            {
                BatchType = BatchType.CardDischarge,
                CompanyId = 111,
                Requester = "Vanessa",
                RequesterEmail = "jairo.goncalves90@gmail.com",
                FilePath = "https://partnermanagementstorage.blob.core.windows.net/pending-batch/629/2018_12_12_20_59_04_863_DESCARGA_CORRUPT.xlsx",
                BatchStatus = BatchStatus.Created
            };
        }


    }
}
