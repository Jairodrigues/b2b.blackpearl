using System;
using System.Collections.Generic;

namespace BlackPearl.Tests.Helpers
{
    public static class MockItemsBatch
    {
        public static Dictionary<string, string> CardRecharge()
        {
            Dictionary<string, string> CardRecharge = new Dictionary<string, string>();
            CardRecharge.Add("CodPrgCrg", "PG20130531");
            CardRecharge.Add("TpIdentif", "1");
            CardRecharge.Add("Identificacao", "5290530200997942");
            CardRecharge.Add("valor", "1");
            CardRecharge.Add("IdRegistro", "");

            return CardRecharge;
        }

        public static Dictionary<string, string> CardDischarge()
        {
            Dictionary<string, string> CardDischarge = new Dictionary<string, string>();
            CardDischarge.Add("Identificacao", "8033302000996961");
            CardDischarge.Add("ValorDescarga", "10");
            return CardDischarge;
        }

        public static Dictionary<string, string> CardIdentification()
        {
            Dictionary<string, string> CardIdentification = new Dictionary<string, string>();
            CardIdentification.Add("TpIdentif", "1");
            CardIdentification.Add("Identificacao", "8033302000999720");
            CardIdentification.Add("CPF", "40242690890");
            CardIdentification.Add("Nome", "Jairo Rodrigues");
            CardIdentification.Add("DtNascimento", "27/10/1993");
            CardIdentification.Add("Sexo", "masculino");
            CardIdentification.Add("CNPJFilial", "1234");
            CardIdentification.Add("Grupo", "Teste");
            CardIdentification.Add("Email", "jairo.goncalves90@gmail.com");
            CardIdentification.Add("DDDCel", "11");
            CardIdentification.Add("Celular", "961474596");
            CardIdentification.Add("NomeMae", "Fania Rocha Gonçalves");
            CardIdentification.Add("IdRegistro", "");
            CardIdentification.Add("Logradouro", "Rua jatauba");
            CardIdentification.Add("Numero", "116");
            CardIdentification.Add("Complemento", "");
            CardIdentification.Add("CEP", "3729140");
            CardIdentification.Add("Cidade", "são paulo");
            CardIdentification.Add("Bairro", "São Paulo");
            CardIdentification.Add("Estado", "São Paulo");
            return CardIdentification;
        }
    }
}