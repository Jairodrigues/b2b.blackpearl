using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BlackPearl.Enums;
using BlackPearl.Model.BatchHeaderFile;
using OfficeOpenXml;
using OfficeOpenXml.Core.ExcelPackage;

//TODO => Tests Unit
namespace BlackPearl.Utils
{
    public static class HandlerFileExcel
    {
        public static List<Dictionary<string, string>> ExtractItemsFileExcel(string filePath, BatchType batchType, string fileType)
        {
            try
            {
                var _filePath = DownloadFile(filePath, fileType);
                return ProcessHeaderAndItems(_filePath, batchType);

            }
            catch (Exception)
            {
                throw;
            }
        }
        private static string DownloadFile(string url, string fileType)
        {
            try
            {
                var _filePath = Path.GetTempFileName().ToString() + fileType;
                using (var client = new WebClient())
                {
                    client.DownloadFile(url, _filePath);
                }
                return _filePath;
            }
            catch (Exception ex)
            {
                throw new FileNotFoundException("Não foi possivel realizar o download", ex);
            }

        }
        private static List<Dictionary<string, string>> ProcessHeaderAndItems(string filePath, BatchType fileType)
        {
            try
            {
                FileInfo file = new FileInfo(filePath);
                OfficeOpenXml.ExcelPackage package = new OfficeOpenXml.ExcelPackage(file);
                OfficeOpenXml.ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                int colCount = GetLastUsedCol(fileType);
                int rowCount = GetLastUsedRow(worksheet);

                List<string> header = FileHeader(colCount, worksheet).GetAwaiter().GetResult();

                List<Dictionary<string, string>> items = FileItems(rowCount, colCount, worksheet, header).GetAwaiter().GetResult();

                if (items.Count == 0)
                {
                    throw new ArgumentException("Arquivo não possui registros");
                }
                else
                {
                    return items;
                }
            }
            catch (DllNotFoundException ex)
            {
                throw new DllNotFoundException("Arquivo Corrompido", ex);
            }
        }
        private static async Task<List<string>> FileHeader(int colCount, OfficeOpenXml.ExcelWorksheet worksheet)
        {
            List<string> header = new List<string>();
            for (int col = 1; col <= colCount; col++)
            {
                var itemText = System.String.Empty;
                itemText = worksheet.Cells[1, col].Text?.ToString();
                if (itemText == "") itemText = "ColunEmpty" + col;
                header.Add(itemText);
            }
            return header;
        }
        private static async Task<List<Dictionary<string, string>>> FileItems(int rowCount, int colCount, OfficeOpenXml.ExcelWorksheet worksheet, List<string> header)
        {
            try
            {
                List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();

                for (int row = 2; row <= rowCount; row++)
                {
                    Dictionary<string, string> listValues = new Dictionary<string, string>();
                    int colHeader = 0;
                    for (int col = 1; col <= colCount; col++)
                    {
                        var itemText = System.String.Empty;
                        itemText = worksheet.Cells[row, col].Text?.ToString();
                        listValues.Add(header[colHeader], itemText);
                        colHeader++;
                    }
                    items.Add(listValues);
                }
                return items;
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException("Arquivo inválido", ex);
            }

        }
        private static int GetLastUsedRow(OfficeOpenXml.ExcelWorksheet worksheet)
        {
            var row = worksheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = worksheet.Cells[row, 1, row, worksheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }
        private static int GetLastUsedCol(BatchType fileType)
        {
            switch (fileType)
            {
                case BatchType.CardRecharge:
                    return typeof(CardRecharge).GetProperties().Count();
                case BatchType.CardIdentification:
                    return typeof(CardIdentification).GetProperties().Count();
                case BatchType.CardDischarge:
                    return typeof(CardDischarge).GetProperties().Count();
                default:
                    return 0;
            }
        }
    }
}
