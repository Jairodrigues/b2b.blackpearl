using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BlackPearl.utils
{
    public class ValidationCPF : ValidationAttribute
    {
        public ValidationCPF()
        {
            ErrorMessage = "CPF deve ser válido.";
        }

        public static string RemoveNotNumber(string text)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^0-9]");
            string ret = reg.Replace(text, string.Empty);
            return ret;
        }

        public override bool IsValid(object cpf)
        {
            cpf = RemoveNotNumber(cpf.ToString());

            if (cpf.ToString().Length > 11)
                return false;

            while (cpf.ToString().Length != 11)
                cpf = '0' + cpf.ToString();

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (cpf.ToString()[i] != cpf.ToString()[0])
                    igual = false;

            if (igual || cpf.ToString() == "12345678909")
                return false;

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++)
                numeros[i] = int.Parse(cpf.ToString()[i].ToString());

            int soma = 0;
            for (int i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            int resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];

            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else
                if (numeros[10] != 11 - resultado)
                return false;

            return true;
        }
    }
}