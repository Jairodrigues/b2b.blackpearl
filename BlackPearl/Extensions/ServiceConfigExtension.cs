using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace BlackPearl.Extensions
{
    public static class ServiceConfigExtension
    {
        /// <summary>
        /// Configura o MVC
        /// </summary>
        /// <param name="services"></param>
        /// <param name="LogPath">Url do elastic search</param>
        public static void MvcConfig(this IServiceCollection services, string LogPath)
        {
            services.AddCors();

            services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            });
        }
    }
}