using System;
using System.Collections.Generic;
using BlackPearl.AzureServiceBus.Common;
using BlackPearl.AzureServiceBus.Queue;
using BlackPearl.Enums;
using BlackPearl.Model.Messages;
using BlackPearl.Model;
using BlackPearl.Repository.Implementations;
using BlackPearl.Services;
using BlackPearl.Services.Implementations;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace BlackPearl.Extensions
{

    public static class StartQueue
    {

        public static void FilePendingQueue(this IServiceProvider serviceProvider)
        {
            var receiver = serviceProvider.GetService<IAzureQueueReceiver<PendingQueue>>();
            var BatchService = serviceProvider.GetService<IBatchService>();
            var BatchItemService = serviceProvider.GetService<IBatchItemService>();

            receiver.Receive(
            message =>
            {
                try
                {
                    var _batch = new Batch()
                    {
                        BatchType = message.BatchType,
                        CompanyId = message.CompanyId,
                        Requester = message.Requester,
                        RequesterEmail = message.RequesterEmail,
                        FilePath = message.FileUrl,
                        BatchStatus = BatchStatus.Created
                    };
                    List<Dictionary<string, string>> _rows = BatchService.Start(_batch);
                    for (int rowNumber = 0; rowNumber <= _rows.Count(); rowNumber++)
                    {
                        BatchItem _batchItem = new BatchItem();
                        _batchItem.LineNumber = rowNumber;
                        _batchItem.BatchId = _batch.Id;
                        _batchItem.Data = _batchItem.DataObject._rows[rowNumber];
                        _batchItem.Status = BatchItemStatus.Created;
                        BatchItemService.Start(_batchItem, _rows[rowNumber]);
                    }

                    return MessageProcessResponse.Complete;
                }
                catch (Exception ex)
                {
                    //logar erro no elasticSearch
                    return MessageProcessResponse.Dead;
                }

            },
            error => { Console.WriteLine("OnError"); },
            () => Console.WriteLine("Waiting..."));

        }
    }
}