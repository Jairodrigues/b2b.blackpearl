using BlackPearl.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPearl.Model
{
    public class BlackPearlContext : DbContext
    {
        private readonly IConfigurationRoot configuration;
        public BlackPearlContext() : base() { }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<BatchItem> BatchItem { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=BlackPearl;User=sa;Password=Acesso@2018");
            //optionsBuilder.UseSqlServer(configuration.GetSection("BlackPearlDatabase").Value);
        }


    }


}