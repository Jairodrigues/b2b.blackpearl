using System;
using BlackPearl.Model;
using BlackPearl.AzureServiceBus.Queue;
using BlackPearl.Repository;
using BlackPearl.Repository.Implementations;
using BlackPearl.Services;
using BlackPearl.Services.Implementations;
using Microsoft.Azure.ServiceBus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BlackPearl.Model.Messages;

namespace BlackPearl.Extensions
{
    public static class Ioc
    {

        public static void Bootstrap(this IServiceCollection services, IConfiguration configuration)
        {

            var AzureServiceBusConnection = configuration["AzureServiceBusConnection"];
            var settingsPendingQueue = new AzureQueueSettings(AzureServiceBusConnection, "testemodulo");
            var policyRetry = new RetryExponential(TimeSpan.FromSeconds(0), TimeSpan.FromHours(2), 8);
            services.AddScoped<IAzureQueueReceiver<PendingQueue>>(x => new AzureQueueReceiver<PendingQueue>(settingsPendingQueue, policyRetry));
            services.AddScoped<UnitOfWork>();

            services.AddScoped<IBatchService, BatchService>();
            services.AddScoped<IBatchItemService, BatchItemService>();
        }
    }
}