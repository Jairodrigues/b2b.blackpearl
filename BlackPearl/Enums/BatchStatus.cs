namespace BlackPearl.Enums
{
    public enum BatchStatus
    {
        Created = 0,
        WaitingForProcessing = 1,
        Processing = 2,
        DoneSuccessfully = 3,
        DoneWithErrors = 4,
        CreatedWithErrors = 5
    }
}