namespace BlackPearl.Enums
{
    public enum BatchItemStatus
    {
        validating = 0,
        Created = 1,
        Processing = 2,
        Success = 3,
        Fail = 4
    }
}