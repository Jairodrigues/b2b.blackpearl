using System.ComponentModel;

namespace BlackPearl.Enums
{
    public enum BatchType
    {
        [Description("recarga")]
        CardRecharge = 0,
        [Description("descarga")]
        CardDischarge = 1,
        [Description("identificação")]
        CardIdentification = 2,
        [Description("bloqueio")]
        Cardlock = 3,
        [Description("desbloqueio")]
        CardUnlock = 4,
        [Description("carga")]
        Charge = 5,
        [Description("carga pessoa fisica")]
        GprCharge = 6,
        [Description("saldo cartao")]
        CardBalance = 7
    }
}