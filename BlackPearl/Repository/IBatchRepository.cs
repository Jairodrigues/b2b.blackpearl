using System.Threading.Tasks;
using BlackPearl.Model;

namespace BlackPearl.Repository
{
    public interface IBatchRepository
    {
        void Create(Batch batch);
        void Update(Batch batch);
    }
}