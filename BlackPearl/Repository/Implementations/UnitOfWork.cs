using System;
using System.Threading.Tasks;
using BlackPearl.Model;
using Microsoft.EntityFrameworkCore;

namespace BlackPearl.Repository.Implementations
{
    public class UnitOfWork
    {
        private readonly BlackPearlContext _context = null;
        private IBatchRepository _batchRepository;
        private IBatchItemRepository _batchItemRepository;

        public UnitOfWork()
        {
            _context = new BlackPearlContext();
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public Task<int> CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public IBatchRepository BatchRepository
        {
            get
            {
                {
                    return _batchRepository ?? (_batchRepository = new BatchRepository(_context));
                }
            }
        }

        public IBatchItemRepository BatchItemRepository
        {
            get
            {
                {
                    return _batchItemRepository ?? (_batchItemRepository = new BatchItemRepository(_context));
                }
            }
        }



    }
}