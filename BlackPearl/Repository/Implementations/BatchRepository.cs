using System;
using System.Threading.Tasks;
using BlackPearl.Extensions;
using BlackPearl.Model;
using Microsoft.EntityFrameworkCore;

//TODO => Tests Unit
namespace BlackPearl.Repository.Implementations
{
    public class BatchRepository : IBatchRepository
    {
        private readonly BlackPearlContext _context = null;

        public BatchRepository(BlackPearlContext context)
        {
            _context = context;
        }
        public void Create(Batch batch)
        {
            _context.Batches.Add(batch);
        }

        public void Update(Batch batch)
        {
            _context.Batches.Update(batch);
        }

    }
}
