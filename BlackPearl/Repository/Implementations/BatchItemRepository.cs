using BlackPearl.Model;

//TODO => Tests Unit
namespace BlackPearl.Repository.Implementations
{
    public class BatchItemRepository : IBatchItemRepository
    {
        private readonly BlackPearlContext _context = null;

        public BatchItemRepository(BlackPearlContext context)
        {
            _context = context;
        }

        public void Create(BatchItem batchItem)
        {
            _context.BatchItem.Add(batchItem);
        }

        public void Update(BatchItem batchItem)
        {
            _context.BatchItem.Update(batchItem);
        }
    }
}