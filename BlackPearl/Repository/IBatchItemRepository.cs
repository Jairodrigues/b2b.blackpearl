using BlackPearl.Model;

namespace BlackPearl.Repository.Implementations
{
    public interface IBatchItemRepository
    {
        void Create(BatchItem batchItem);
        void Update(BatchItem batchItem);
    }
}