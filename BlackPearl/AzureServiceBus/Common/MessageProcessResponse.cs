﻿namespace BlackPearl.AzureServiceBus.Common
{
    public enum MessageProcessResponse
    {
        Complete,
        Abandon,
        Dead
    }
}
