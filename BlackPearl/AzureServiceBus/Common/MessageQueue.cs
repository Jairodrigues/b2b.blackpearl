﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BlackPearl.AzureServiceBus.Common
{
    public abstract class MessageQueue
    {
        [JsonIgnore]
        public IDictionary<string, object> UserProperties { get; set; }
    }
}
