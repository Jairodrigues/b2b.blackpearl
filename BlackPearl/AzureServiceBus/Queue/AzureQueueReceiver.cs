﻿using BlackPearl.AzureServiceBus.Common;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;

namespace BlackPearl.AzureServiceBus.Queue
{
    public class AzureQueueReceiver<T> : IAzureQueueReceiver<T> where T : MessageQueue
    {
        private QueueClient client;
        private AzureQueueSettings settings;
        public AzureQueueReceiver(AzureQueueSettings settings, RetryExponential retryPolicy = null)
        {
            this.settings = settings;
            Init(retryPolicy);
        }


        public void Receive(Func<T, MessageProcessResponse> onProcess, Action<Exception> onError, Action onWait)
        {
            var options = new MessageHandlerOptions(e =>
            {
                onError?.Invoke(e.Exception);
                return Task.CompletedTask;
            })
            {
                AutoComplete = false,
                MaxAutoRenewDuration = TimeSpan.FromMinutes(1)
            };

            client.RegisterMessageHandler(
                async (message, token) =>
                {
                    try
                    {
                        var data = Encoding.UTF8.GetString(message.Body);
                        var item = JsonConvert.DeserializeObject<T>(data);
                        item.UserProperties = message.UserProperties;
                        var result = onProcess?.Invoke(item) ?? default(MessageProcessResponse);

                        switch (result)
                        {
                            case MessageProcessResponse.Complete:
                                await client.CompleteAsync(message.SystemProperties.LockToken);
                                break;
                            case MessageProcessResponse.Abandon:
                                await client.AbandonAsync(message.SystemProperties.LockToken);
                                break;
                            case MessageProcessResponse.Dead:
                                await client.DeadLetterAsync(message.SystemProperties.LockToken);
                                break;
                            default:
                                throw new Exception("Unexpected Case");
                        }
                        // TODO: Verificar se abaixo o nivel de consumo de memoria e processador.
                        // onWait?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        if (message.SystemProperties.DeliveryCount > ((RetryExponential)client.RetryPolicy).MaxRetryCount)
                            await client.DeadLetterAsync(message.SystemProperties.LockToken);
                        else
                            await client.AbandonAsync(message.SystemProperties.LockToken);
                        onError?.Invoke(ex);
                    }
                }, options);
        }

        private void Init(RetryExponential retryPolicy)
        {
            client = new QueueClient(settings.ConnectionString, settings.QueueName, ReceiveMode.PeekLock, retryPolicy);
        }

    }
}
