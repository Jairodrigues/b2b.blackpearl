﻿using BlackPearl.AzureServiceBus.Common;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackPearl.AzureServiceBus.Queue
{
    public class AzureQueueSender<T> : IAzureQueueSender<T> where T : MessageQueue
    {
        public AzureQueueSender(AzureQueueSettings settings)
        {
            this.settings = settings;
            Init();
        }

        public async Task SendAsync(T item)
        {
            var json = JsonConvert.SerializeObject(item);
            var message = new Message(Encoding.UTF8.GetBytes(json));

            if (item.UserProperties != null)
                foreach (var prop in item.UserProperties)
                    message.UserProperties.Add(prop.Key, prop.Value);

            await client.SendAsync(message);
        }

        private AzureQueueSettings settings;
        private QueueClient client;

        private void Init()
        {
            client = new QueueClient(this.settings.ConnectionString, this.settings.QueueName);
        }

    }
}
