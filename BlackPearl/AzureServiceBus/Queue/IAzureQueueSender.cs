﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackPearl.AzureServiceBus.Queue
{
    public interface IAzureQueueSender<T>
    {
        Task SendAsync(T item);
    }
}
