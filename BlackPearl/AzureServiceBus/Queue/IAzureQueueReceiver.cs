﻿using BlackPearl.AzureServiceBus.Common;
using Microsoft.Extensions.Hosting;
using System;

namespace BlackPearl.AzureServiceBus.Queue
{
    public interface IAzureQueueReceiver<T> where T : MessageQueue
    {
        void Receive(
            Func<T, MessageProcessResponse> onProcess,
            Action<Exception> onError,
            Action onWait);
    }
}
