using System;
using System.ComponentModel.DataAnnotations.Schema;
using BlackPearl.Enums;
using BlackPearl.Repository;

namespace BlackPearl.Model
{
    public class Batch
    {
        public long Id { get; set; }
        public string Requester { get; set; }
        public string RequesterEmail { get; set; }
        public int BatchItemCount { get; set; }
        public long CompanyId { get; set; }
        public string FilePath { get; set; }
        public BatchStatus BatchStatus { get; set; }
        public BatchType BatchType { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorStackTrace { get; set; }
        public int BatchItemCountSucess { get; set; }
        public int BatchItemCountFail { get; set; }
    }
}