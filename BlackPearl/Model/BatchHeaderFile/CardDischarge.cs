namespace BlackPearl.Model.BatchHeaderFile
{
    public class CardDischarge
    {
        public string Identificacao { get; set; }
        public decimal ValorDescarga { get; set; }
    }
}