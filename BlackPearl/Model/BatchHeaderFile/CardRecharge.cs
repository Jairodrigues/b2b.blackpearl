using System;

namespace BlackPearl.Model.BatchHeaderFile
{
    public class CardRecharge
    {
        public string CodPrgCrg { get; set; }
        public int TpIdentif { get; set; }
        public string Identificacao { get; set; }
        public Decimal Valor { get; set; }
        public long IdRegistro { get; set; }
    }
}