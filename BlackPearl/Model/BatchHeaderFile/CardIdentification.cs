using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BlackPearl.utils;

namespace BlackPearl.Model.BatchHeaderFile
{
    public class CardIdentification
    {
        [Required]
        [Range(1, 2, ErrorMessage = "Identificador {0} não encontrado, utilize {1} ou {2}.")]
        public string TypeIdentification { get; set; }

        [Required]
        public string Identification { get; set; }

        [Required(ErrorMessage = "CPF deve ser preenchido.")]
        [ValidationCPF]
        public string Document { get; set; }

        [Required]
        public string Name { get; set; }

        public static CardIdentification isValid(Dictionary<string, string> row)
        {
            try
            {
                var validationRow = new CardIdentification
                {
                    TypeIdentification = row[nameof(TypeIdentification)],
                    Identification = row[nameof(Identification)],
                    Document = row[nameof(Document)],
                    Name = row[nameof(Name)]
                };
                return validationRow;
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Linha não processada,", ex);
            }

        }

    }


}