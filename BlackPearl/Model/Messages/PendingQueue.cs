using BlackPearl.AzureServiceBus.Common;
using BlackPearl.Enums;

namespace BlackPearl.Model.Messages
{
    public class PendingQueue : MessageQueue
    {
        public string FileUrl { get; set; }
        public string Requester { get; set; }
        public string RequesterEmail { get; set; }
        public BatchType BatchType { get; set; }
        public int CompanyId { get; set; }
    }
}