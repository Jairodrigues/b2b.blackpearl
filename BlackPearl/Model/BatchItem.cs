using System.ComponentModel.DataAnnotations.Schema;
using BlackPearl.Enums;
using BlackPearl.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BlackPearl.Model
{
    public class BatchItem
    {
        public long Id { get; set; }
        public long BatchId { get; set; }
        public BatchItemStatus Status { get; set; }
        public string Data { get; set; }
        public string ErrorMessage { get; set; }
        public string StackTrace { get; set; }
        public int LineNumber { get; set; }

        // A property DataObject é salvo dentro da coluna Data com as informações dos arquivos
        [NotMapped]
        public dynamic DataObject
        {
            // deserializa o documento json salvo como string
            get => JsonConvert.DeserializeObject<dynamic>(Data);
            // serializa documento recebido como string e salva na coluna data
            set
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                Data = JsonConvert.SerializeObject(value);
            }
        }
    }
}