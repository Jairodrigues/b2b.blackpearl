using System;
using System.Collections.Generic;
using System.Linq;
using BlackPearl.Enums;
using BlackPearl.Model;
using BlackPearl.Model.BatchHeaderFile;
using BlackPearl.Repository;
using BlackPearl.Repository.Implementations;

//TODO => Tests Unit
//TODO => tests Integration
namespace BlackPearl.Services.Implementations
{
    public class BatchItemService : IBatchItemService
    {
        private readonly UnitOfWork _uow;

        public BatchItemService(UnitOfWork uow)
        {
            _uow = uow;
        }

        public void Start(BatchItem batchItem, Dictionary<string, string> line)
        {
            try
            {
                CreateBatchItem(batchItem);
                var _lineValidate = CardIdentification.isValid(line);
                batchItem.Data = batchItem.DataObject._lineValidate;
                UpdateBatchItem(batchItem);
            }
            catch (Exception ex)
            {
                batchItem.Status = BatchItemStatus.Processing;
                batchItem.StackTrace = ex.StackTrace;
                batchItem.ErrorMessage = ex.Message;
                UpdateBatchItem(batchItem);
                throw;
            }

        }

        public void CreateBatchItem(BatchItem batchItem)
        {
            _uow.BatchItemRepository.Create(batchItem);
            _uow.CommitAsync();
        }

        public void UpdateBatchItem(BatchItem batchItem)
        {
            batchItem.Status = BatchItemStatus.validating;
            _uow.BatchItemRepository.Update(batchItem);
            _uow.CommitAsync();
        }

    }
}


