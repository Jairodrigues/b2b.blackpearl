using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlackPearl.AzureServiceBus.Common;
using BlackPearl.Enums;
using BlackPearl.Model;
using BlackPearl.Repository;
using BlackPearl.Repository.Implementations;
using BlackPearl.Utils;

//TODO => Tests Unit
//TODO => tests Integration
// teste de criação de cada tipo de acao 
// teste de criação com arquivos invalidos
// teste de criação com possiveis erros de banco
// teste de criação com possiveis erros de fila
// teste de stress (varios request)

namespace BlackPearl.Services.Implementations
{
    public class BatchService : IBatchService
    {

        private readonly UnitOfWork _uow;

        public BatchService(UnitOfWork uow)
        {
            _uow = uow;
        }


        public List<Dictionary<string, string>> Start(Batch batch)
        {
            try
            {
                CreateBatch(batch);
                string _filePath = FileType(batch.FilePath);
                return HandlerFileExcel.ExtractItemsFileExcel(batch.FilePath, batch.BatchType, _filePath);
            }
            catch (Exception ex)
            {
                batch.BatchStatus = BatchStatus.CreatedWithErrors;
                batch.ErrorMessage = ex.Message;
                batch.ErrorStackTrace = ex.StackTrace;
                UpdateBatch(batch);
                throw;
            }
        }

        public static string FileType(string filePath)
        {
            var _fileType = filePath.Substring(filePath.LastIndexOf("."));
            if (_fileType == ".xlsx" || _fileType == ".xls" || _fileType == ".csv")
            {
                return _fileType;
            }
            else
            {
                throw new ArgumentException("Extensão utilizada não suportada");
            }
        }

        public void CreateBatch(Batch batch)
        {
            batch.BatchStatus = BatchStatus.Created;
            batch.CreatedAt = DateTime.Now;
            batch.UpdatedAt = batch.CreatedAt;
            _uow.BatchRepository.Create(batch);
            _uow.CommitAsync();
        }

        public void UpdateBatch(Batch batch)
        {
            _uow.BatchRepository.Update(batch);
            _uow.CommitAsync();
        }


    }
}