using System.Collections.Generic;
using System.Threading.Tasks;
using BlackPearl.Model;

namespace BlackPearl.Services
{
    public interface IBatchService
    {
        List<Dictionary<string, string>> Start(Batch batch);
        void CreateBatch(Batch batch);
        void UpdateBatch(Batch batch);


    }
}