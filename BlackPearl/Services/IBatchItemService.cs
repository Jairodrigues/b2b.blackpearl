using System.Collections.Generic;
using BlackPearl.Enums;
using BlackPearl.Model;
using BlackPearl.Model.BatchHeaderFile;

namespace BlackPearl.Services.Implementations
{
    public interface IBatchItemService
    {
        void Start(BatchItem batchItem, Dictionary<string, string> line);
        void CreateBatchItem(BatchItem batchItem);
        void UpdateBatchItem(BatchItem batchItem);
    }

}